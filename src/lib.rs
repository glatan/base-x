#![no_std]

extern crate alloc;

use alloc::string::String;

pub trait Encoder {
    fn encode(input: impl AsRef<[u8]>) -> String;
}

mod base64;

pub use base64::Base64;
