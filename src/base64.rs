use alloc::string::String;

use crate::Encoder;

const LOOKUP_TABLE: [char; 64] = [
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S',
    'T', 'U', 'V', 'W', 'x', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l',
    'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4',
    '5', '6', '7', '8', '9', '+', '/',
];

const PADDING_VALUE: char = '=';

pub struct Base64;

impl Base64 {
    const MIN_GROUP_SIZE: usize = 3; // 24 bit
}

impl Encoder for Base64 {
    fn encode(input: impl AsRef<[u8]>) -> String {
        let iter = input.as_ref().chunks_exact(Self::MIN_GROUP_SIZE);
        let remainder = iter.remainder();
        let mut result = String::with_capacity(input.as_ref().len());
        for group in iter {
            result.push(LOOKUP_TABLE[(group[0] >> 2) as usize]);
            result.push(
                LOOKUP_TABLE
                    [(((group[0] << 4) & 0b0011_0000) | ((group[1] >> 4) & 0b0000_1111)) as usize],
            );
            result.push(
                LOOKUP_TABLE
                    [(((group[1] << 2) & 0b0011_1100) | ((group[2] >> 6) & 0b0000_0011)) as usize],
            );
            result.push(LOOKUP_TABLE[(group[2] & 0b0011_1111) as usize]);
        }
        match remainder.len() {
            1 => {
                result.push(LOOKUP_TABLE[(remainder[0] >> 2) as usize]);
                result.push(LOOKUP_TABLE[((remainder[0] << 4) & 0b0011_0000) as usize]);
            }
            2 => {
                result.push(LOOKUP_TABLE[(remainder[0] >> 2) as usize]);
                result.push(
                    LOOKUP_TABLE[(((remainder[0] << 4) & 0b0011_0000)
                        | ((remainder[1] >> 4) & 0b0000_1111))
                        as usize],
                );
                result.push(LOOKUP_TABLE[((remainder[1] << 2) & 0b0011_1100) as usize]);
            }
            _ => (),
        }
        if result.len() % 4 != 0 {
            for _ in 0..(4 - result.len() % 4) {
                result.push(PADDING_VALUE);
            }
        }
        assert!(result.len() % 4 == 0);
        result
    }
}

#[cfg(test)]
mod tests {
    use crate::Encoder;

    use super::Base64;

    const TEST_CASE: [(&'static str, &'static str); 7] = [
        ("", ""),
        ("f", "Zg=="),
        ("fo", "Zm8="),
        ("foo", "Zm9v"),
        ("foob", "Zm9vYg=="),
        ("fooba", "Zm9vYmE="),
        ("foobar", "Zm9vYmFy"),
    ];
    #[test]
    fn encode() {
        for (input, expected) in TEST_CASE {
            let actual = Base64::encode(input);
            assert_eq!(expected, actual);
        }
    }
}
