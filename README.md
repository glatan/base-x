# Base-X

[RFC4648](https://www.rfc-editor.org/rfc/rfc4648)形式のBase16, Base32, Base64の実装

## 実装状況

- [ ] Base16
    - [ ] Encoder
    - [ ] Decoder
- [ ] Base32
    - [ ] Encoder
    - [ ] Decoder
- [ ] Base64
    - [x] Encoder
    - [ ] Decoder
